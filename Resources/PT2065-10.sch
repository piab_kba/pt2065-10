EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "10 Button controller"
Date "2021-02-18"
Rev "C"
Comp "TAWI"
Comment1 "Updatec from POC to Prototyp with smd comps."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switches:SW_Push_Dual_tactile SW6
U 1 1 5FBE401C
P 3450 2350
F 0 "SW6" H 3450 2650 50  0000 C CNN
F 1 "Button6" H 3450 2550 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 3450 2650 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3450 2650 50  0001 C CNN
	1    3450 2350
	-1   0    0    -1  
$EndComp
Text GLabel 5450 4350 0    50   Input ~ 0
KEYSWITCH
$Comp
L power:GND #PWR012
U 1 1 5FBE6F36
P 6150 4550
F 0 "#PWR012" H 6150 4300 50  0001 C CNN
F 1 "GND" H 6155 4377 50  0000 C CNN
F 2 "" H 6150 4550 50  0001 C CNN
F 3 "" H 6150 4550 50  0001 C CNN
	1    6150 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 602E266A
P 9600 1750
F 0 "D1" H 9593 1967 50  0000 C CNN
F 1 "Indicator" H 9593 1876 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9600 1750 50  0001 C CNN
F 3 "~" H 9600 1750 50  0001 C CNN
	1    9600 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 602E5730
P 10000 1750
F 0 "R2" V 9804 1750 50  0000 C CNN
F 1 "510" V 9895 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 10000 1750 50  0001 C CNN
F 3 "~" H 10000 1750 50  0001 C CNN
	1    10000 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 4450 6150 4550
Wire Wire Line
	5850 4450 6150 4450
Text GLabel 6150 2050 2    50   Input ~ 0
INDICATOR
Text GLabel 6150 2150 2    50   Input ~ 0
Row2
Text GLabel 6150 2250 2    50   Input ~ 0
Column2
Text GLabel 6150 2350 2    50   Input ~ 0
Column4
Text GLabel 5150 2650 0    50   Input ~ 0
KEYSWITCH
$Comp
L power:GND #PWR0101
U 1 1 602F2308
P 6250 2750
F 0 "#PWR0101" H 6250 2500 50  0001 C CNN
F 1 "GND" H 6255 2577 50  0000 C CNN
F 2 "" H 6250 2750 50  0001 C CNN
F 3 "" H 6250 2750 50  0001 C CNN
	1    6250 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 2750 6250 2650
Wire Wire Line
	6250 2650 6150 2650
Text GLabel 5150 2450 0    50   Input ~ 0
Column5
Text GLabel 5150 2350 0    50   Input ~ 0
Column3
Text GLabel 5150 2250 0    50   Input ~ 0
Column1
Text GLabel 5150 2150 0    50   Input ~ 0
Row1
$Comp
L Switch:SW_Push_DPDT SW11
U 1 1 5FBE5CA0
P 5650 4550
F 0 "SW11" H 5650 5035 50  0000 C CNN
F 1 "Keyswitch" H 5650 4944 50  0000 C CNN
F 2 "Button_Switch_THT:SW_CuK_JS202011AQN_DPDT_Angled" H 5650 4750 50  0001 C CNN
F 3 "~" H 5650 4750 50  0001 C CNN
	1    5650 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2050 5050 1950
$Comp
L Transistor_BJT:BCP56 Q1
U 1 1 6030D326
P 9150 4450
F 0 "Q1" H 9340 4496 50  0000 L CNN
F 1 "BCP54-16" H 9340 4405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 9350 4375 50  0001 L CIN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bc337.pdf" H 9150 4450 50  0001 L CNN
	1    9150 4450
	1    0    0    -1  
$EndComp
Text GLabel 8400 4450 0    50   Input ~ 0
INDICATOR
$Comp
L power:GND #PWR013
U 1 1 6031F7EF
P 9250 4800
F 0 "#PWR013" H 9250 4550 50  0001 C CNN
F 1 "GND" H 9255 4627 50  0000 C CNN
F 2 "" H 9250 4800 50  0001 C CNN
F 3 "" H 9250 4800 50  0001 C CNN
	1    9250 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 4650 9250 4750
Wire Wire Line
	8800 4450 8850 4450
Wire Wire Line
	8400 4450 8600 4450
$Comp
L power:+24V #PWR011
U 1 1 6030A7C5
P 5050 1950
F 0 "#PWR011" H 5050 1800 50  0001 C CNN
F 1 "+24V" H 5065 2123 50  0000 C CNN
F 2 "" H 5050 1950 50  0001 C CNN
F 3 "" H 5050 1950 50  0001 C CNN
	1    5050 1950
	1    0    0    -1  
$EndComp
Text GLabel 9250 4050 1    50   Input ~ 0
LED
Wire Wire Line
	9250 4250 9250 4050
$Comp
L power:+24V #PWR014
U 1 1 6032365B
P 10400 1700
F 0 "#PWR014" H 10400 1550 50  0001 C CNN
F 1 "+24V" H 10415 1873 50  0000 C CNN
F 2 "" H 10400 1700 50  0001 C CNN
F 3 "" H 10400 1700 50  0001 C CNN
	1    10400 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 6032705F
P 9150 1750
F 0 "D2" H 9143 1967 50  0000 C CNN
F 1 "Indicator" H 9143 1876 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9150 1750 50  0001 C CNN
F 3 "~" H 9150 1750 50  0001 C CNN
	1    9150 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 603273EE
P 8700 1750
F 0 "D3" H 8693 1967 50  0000 C CNN
F 1 "Indicator" H 8693 1876 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8700 1750 50  0001 C CNN
F 3 "~" H 8700 1750 50  0001 C CNN
	1    8700 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 60327991
P 8250 1750
F 0 "D4" H 8243 1967 50  0000 C CNN
F 1 "Indicator" H 8243 1876 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8250 1750 50  0001 C CNN
F 3 "~" H 8250 1750 50  0001 C CNN
	1    8250 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 60327B72
P 7800 1750
F 0 "D5" H 7793 1967 50  0000 C CNN
F 1 "Indicator" H 7793 1876 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7800 1750 50  0001 C CNN
F 3 "~" H 7800 1750 50  0001 C CNN
	1    7800 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D6
U 1 1 603282DA
P 7800 2100
F 0 "D6" H 7793 2317 50  0000 C CNN
F 1 "Indicator" H 7793 2226 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7800 2100 50  0001 C CNN
F 3 "~" H 7800 2100 50  0001 C CNN
	1    7800 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D7
U 1 1 60328C21
P 8250 2100
F 0 "D7" H 8243 2317 50  0000 C CNN
F 1 "Indicator" H 8243 2226 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8250 2100 50  0001 C CNN
F 3 "~" H 8250 2100 50  0001 C CNN
	1    8250 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D8
U 1 1 60328EBA
P 8700 2100
F 0 "D8" H 8693 2317 50  0000 C CNN
F 1 "Indicator" H 8693 2226 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8700 2100 50  0001 C CNN
F 3 "~" H 8700 2100 50  0001 C CNN
	1    8700 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D9
U 1 1 6032909B
P 9150 2100
F 0 "D9" H 9143 2317 50  0000 C CNN
F 1 "Indicator" H 9143 2226 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9150 2100 50  0001 C CNN
F 3 "~" H 9150 2100 50  0001 C CNN
	1    9150 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D10
U 1 1 603293B1
P 9600 2100
F 0 "D10" H 9593 2317 50  0000 C CNN
F 1 "Indicator" H 9593 2226 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9600 2100 50  0001 C CNN
F 3 "~" H 9600 2100 50  0001 C CNN
	1    9600 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	9000 1750 8850 1750
Wire Wire Line
	8550 1750 8400 1750
Wire Wire Line
	8100 1750 7950 1750
Wire Wire Line
	7650 1750 7550 1750
Wire Wire Line
	7550 1750 7550 2100
Wire Wire Line
	7550 2100 7650 2100
Wire Wire Line
	7950 2100 8100 2100
Wire Wire Line
	8400 2100 8550 2100
Wire Wire Line
	8850 2100 9000 2100
Wire Wire Line
	9300 2100 9450 2100
Wire Wire Line
	9900 1750 9750 1750
Wire Wire Line
	9450 1750 9300 1750
Text GLabel 9900 2100 2    50   Input ~ 0
LED
Wire Wire Line
	9900 2100 9750 2100
Wire Wire Line
	10400 1700 10400 1750
Wire Wire Line
	10400 1750 10100 1750
$Comp
L Device:R_Small R1
U 1 1 6030EDB8
P 8700 4450
F 0 "R1" V 8504 4450 50  0000 C CNN
F 1 "10k" V 8595 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 8700 4450 50  0001 C CNN
F 3 "~" H 8700 4450 50  0001 C CNN
	1    8700 4450
	0    -1   -1   0   
$EndComp
$Comp
L 8-215079-4:8-215079-4 J1
U 1 1 602F12C0
P 5650 2350
F 0 "J1" H 5650 2950 50  0000 C CNN
F 1 "Handset" H 5650 2850 50  0000 C CNN
F 2 "8-215079-4:TE_8-215079-4" H 5650 2350 50  0001 L BNN
F 3 "" H 5650 2350 50  0001 L BNN
F 4 "Manufacturer Recommendation" H 5650 2350 50  0001 L BNN "STANDARD"
F 5 "TE Connectivity" H 5650 2350 50  0001 L BNN "MANUFACTURER"
F 6 "S4" H 5650 2350 50  0001 L BNN "PARTREV"
	1    5650 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2050 5150 2050
NoConn ~ 5850 4250
NoConn ~ 5450 4750
NoConn ~ 5850 4650
NoConn ~ 5850 4850
$Comp
L Device:R_Small R3
U 1 1 60B8B043
P 9000 4750
F 0 "R3" V 8804 4750 50  0000 C CNN
F 1 "5k" V 8895 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 9000 4750 50  0001 C CNN
F 3 "~" H 9000 4750 50  0001 C CNN
	1    9000 4750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8850 4450 8850 4750
Wire Wire Line
	8850 4750 8900 4750
Connection ~ 8850 4450
Wire Wire Line
	8850 4450 8950 4450
Wire Wire Line
	9100 4750 9250 4750
Connection ~ 9250 4750
Wire Wire Line
	9250 4750 9250 4800
NoConn ~ 6150 2550
NoConn ~ 5150 2550
Text GLabel 2300 2750 0    50   Input ~ 0
Column1
Text GLabel 3050 2350 0    50   Input ~ 0
Row2
$Comp
L power:+5V #PWR015
U 1 1 61A6D4E9
P 6750 2450
F 0 "#PWR015" H 6750 2300 50  0001 C CNN
F 1 "+5V" H 6765 2623 50  0000 C CNN
F 2 "" H 6750 2450 50  0001 C CNN
F 3 "" H 6750 2450 50  0001 C CNN
	1    6750 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2450 6150 2450
NoConn ~ 3650 2550
NoConn ~ 3650 2350
$Comp
L Switches:SW_Push_Dual_tactile SW1
U 1 1 61A87FE3
P 1550 2350
F 0 "SW1" H 1550 2650 50  0000 C CNN
F 1 "Button1" H 1550 2550 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 1550 2650 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 1550 2650 50  0001 C CNN
	1    1550 2350
	1    0    0    -1  
$EndComp
Text GLabel 1950 2350 2    50   Input ~ 0
Row1
NoConn ~ 1350 2350
NoConn ~ 1350 2550
$Comp
L Device:D_x2_KCom_AAK D11
U 1 1 61B3A80C
P 2500 2550
F 0 "D11" H 2500 2767 50  0000 C CNN
F 1 "450mA" H 2500 2676 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 2500 2550 50  0001 C CNN
F 3 "~" H 2500 2550 50  0001 C CNN
	1    2500 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2550 2800 2550
Wire Wire Line
	2200 2550 1750 2550
Wire Wire Line
	2500 2750 2300 2750
$Comp
L Switches:SW_Push_Dual_tactile SW7
U 1 1 61B4F784
P 3450 3250
F 0 "SW7" H 3450 3550 50  0000 C CNN
F 1 "Button7" H 3450 3450 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 3450 3550 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3450 3550 50  0001 C CNN
	1    3450 3250
	-1   0    0    -1  
$EndComp
Text GLabel 2300 3650 0    50   Input ~ 0
Column2
Text GLabel 3050 3250 0    50   Input ~ 0
Row2
NoConn ~ 3650 3450
NoConn ~ 3650 3250
$Comp
L Switches:SW_Push_Dual_tactile SW2
U 1 1 61B4FA56
P 1550 3250
F 0 "SW2" H 1550 3550 50  0000 C CNN
F 1 "Button2" H 1550 3450 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 1550 3550 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 1550 3550 50  0001 C CNN
	1    1550 3250
	1    0    0    -1  
$EndComp
Text GLabel 1950 3250 2    50   Input ~ 0
Row1
NoConn ~ 1350 3250
NoConn ~ 1350 3450
$Comp
L Device:D_x2_KCom_AAK D12
U 1 1 61B4FA7D
P 2500 3450
F 0 "D12" H 2500 3667 50  0000 C CNN
F 1 "450mA" H 2500 3576 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 2500 3450 50  0001 C CNN
F 3 "~" H 2500 3450 50  0001 C CNN
	1    2500 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3450 2800 3450
Wire Wire Line
	2200 3450 1750 3450
Wire Wire Line
	2500 3650 2300 3650
$Comp
L Switches:SW_Push_Dual_tactile SW8
U 1 1 61B59878
P 3450 4150
F 0 "SW8" H 3450 4450 50  0000 C CNN
F 1 "Button8" H 3450 4350 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 3450 4450 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3450 4450 50  0001 C CNN
	1    3450 4150
	-1   0    0    -1  
$EndComp
Text GLabel 2300 4550 0    50   Input ~ 0
Column3
Text GLabel 3050 4150 0    50   Input ~ 0
Row2
NoConn ~ 3650 4350
NoConn ~ 3650 4150
$Comp
L Switches:SW_Push_Dual_tactile SW3
U 1 1 61B59BEC
P 1550 4150
F 0 "SW3" H 1550 4450 50  0000 C CNN
F 1 "Button3" H 1550 4350 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 1550 4450 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 1550 4450 50  0001 C CNN
	1    1550 4150
	1    0    0    -1  
$EndComp
Text GLabel 1950 4150 2    50   Input ~ 0
Row1
NoConn ~ 1350 4150
NoConn ~ 1350 4350
$Comp
L Device:D_x2_KCom_AAK D13
U 1 1 61B59C13
P 2500 4350
F 0 "D13" H 2500 4567 50  0000 C CNN
F 1 "450mA" H 2500 4476 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 2500 4350 50  0001 C CNN
F 3 "~" H 2500 4350 50  0001 C CNN
	1    2500 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 4350 2800 4350
Wire Wire Line
	2200 4350 1750 4350
Wire Wire Line
	2500 4550 2300 4550
$Comp
L Switches:SW_Push_Dual_tactile SW9
U 1 1 61B59C20
P 3450 5050
F 0 "SW9" H 3450 5350 50  0000 C CNN
F 1 "Button9" H 3450 5250 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 3450 5350 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3450 5350 50  0001 C CNN
	1    3450 5050
	-1   0    0    -1  
$EndComp
Text GLabel 2300 5450 0    50   Input ~ 0
Column4
Text GLabel 3050 5050 0    50   Input ~ 0
Row2
NoConn ~ 3650 5250
NoConn ~ 3650 5050
$Comp
L Switches:SW_Push_Dual_tactile SW4
U 1 1 61B59C48
P 1550 5050
F 0 "SW4" H 1550 5350 50  0000 C CNN
F 1 "Button4" H 1550 5250 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 1550 5350 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 1550 5350 50  0001 C CNN
	1    1550 5050
	1    0    0    -1  
$EndComp
Text GLabel 1950 5050 2    50   Input ~ 0
Row1
NoConn ~ 1350 5050
NoConn ~ 1350 5250
$Comp
L Device:D_x2_KCom_AAK D14
U 1 1 61B59C6F
P 2500 5250
F 0 "D14" H 2500 5467 50  0000 C CNN
F 1 "450mA" H 2500 5376 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 2500 5250 50  0001 C CNN
F 3 "~" H 2500 5250 50  0001 C CNN
	1    2500 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 5250 2800 5250
Wire Wire Line
	2200 5250 1750 5250
Wire Wire Line
	2500 5450 2300 5450
$Comp
L Switches:SW_Push_Dual_tactile SW10
U 1 1 61B646B2
P 3450 5950
F 0 "SW10" H 3450 6250 50  0000 C CNN
F 1 "Button10" H 3450 6150 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 3450 6250 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 3450 6250 50  0001 C CNN
	1    3450 5950
	-1   0    0    -1  
$EndComp
Text GLabel 2300 6350 0    50   Input ~ 0
Column5
Text GLabel 3050 5950 0    50   Input ~ 0
Row2
NoConn ~ 3650 6150
NoConn ~ 3650 5950
$Comp
L Switches:SW_Push_Dual_tactile SW5
U 1 1 61B64B6A
P 1550 5950
F 0 "SW5" H 1550 6250 50  0000 C CNN
F 1 "Button5" H 1550 6150 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_Omron_B3FS-100xP" H 1550 6250 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=1371" H 1550 6250 50  0001 C CNN
	1    1550 5950
	1    0    0    -1  
$EndComp
Text GLabel 1950 5950 2    50   Input ~ 0
Row1
NoConn ~ 1350 5950
NoConn ~ 1350 6150
$Comp
L Device:D_x2_KCom_AAK D15
U 1 1 61B64B91
P 2500 6150
F 0 "D15" H 2500 6367 50  0000 C CNN
F 1 "450mA" H 2500 6276 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 2500 6150 50  0001 C CNN
F 3 "~" H 2500 6150 50  0001 C CNN
	1    2500 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 6150 2800 6150
Wire Wire Line
	2200 6150 1750 6150
Wire Wire Line
	2500 6350 2300 6350
Text GLabel 2700 1300 2    50   Input ~ 0
Row1
$Comp
L Device:R_Small R4
U 1 1 61BACED1
P 2450 1300
F 0 "R4" V 2254 1300 50  0000 C CNN
F 1 "4,3K" V 2345 1300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2450 1300 50  0001 C CNN
F 3 "~" H 2450 1300 50  0001 C CNN
	1    2450 1300
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 61BAD20A
P 2200 1300
F 0 "#PWR0102" H 2200 1150 50  0001 C CNN
F 1 "+5V" H 2215 1473 50  0000 C CNN
F 2 "" H 2200 1300 50  0001 C CNN
F 3 "" H 2200 1300 50  0001 C CNN
	1    2200 1300
	1    0    0    -1  
$EndComp
Text GLabel 2700 1500 2    50   Input ~ 0
Row2
$Comp
L Device:R_Small R5
U 1 1 61BAD681
P 2450 1500
F 0 "R5" V 2254 1500 50  0000 C CNN
F 1 "4,3K" V 2345 1500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2450 1500 50  0001 C CNN
F 3 "~" H 2450 1500 50  0001 C CNN
	1    2450 1500
	0    1    -1   0   
$EndComp
Wire Wire Line
	2200 1300 2250 1300
Wire Wire Line
	2250 1300 2250 1500
Wire Wire Line
	2250 1500 2350 1500
Connection ~ 2250 1300
Wire Wire Line
	2250 1300 2350 1300
Wire Wire Line
	2550 1300 2700 1300
Wire Wire Line
	2550 1500 2700 1500
Wire Wire Line
	1750 2350 1950 2350
Wire Wire Line
	3050 2350 3250 2350
Wire Wire Line
	1750 3250 1950 3250
Wire Wire Line
	3050 3250 3250 3250
Wire Wire Line
	1750 4150 1950 4150
Wire Wire Line
	3050 4150 3250 4150
Wire Wire Line
	3050 5050 3250 5050
Wire Wire Line
	1750 5050 1950 5050
Wire Wire Line
	3050 5950 3250 5950
Wire Wire Line
	1750 5950 1950 5950
$EndSCHEMATC
